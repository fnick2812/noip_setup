#!/usr/bin/env bash

export MY_USERNAME=<foo>
export MY_REPO_FOLDER=<foo_repo_path>

##
# install noip
wget http://www.noip.com/client/linux/noip-duc-linux.tar.gz
tar xf noip-duc-linux.tar.gz
cd noip-2.1.9-1/
sudo make install

## 
# generate noip config
# use `noip2 -C` to generate config again
# or use installer/noip/no-ip2.conf

##
# register mister with systemd
cd $MY_REPO_FOLDER
cp noip.service.template noip.service
sed -i "s+__MY_REPO_FOLDER__+$MY_REPO_FOLDER+g" noip.service
cp noip.service /etc/systemd/system
systemctl enable noip.service
systemctl daemon-reload